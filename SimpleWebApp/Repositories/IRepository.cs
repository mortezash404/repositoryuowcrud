﻿using System.Threading.Tasks;

namespace SimpleWebApp.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<T> GetAsync<T>(object id) where T : class;
        Task InsertAsync<T>(T entity) where T : class;
        Task UpdateAsync<T>(T entity) where T : class;
        Task DeleteAsync(object id);
    }
}