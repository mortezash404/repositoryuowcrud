﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SimpleWebApp.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly AppDbContext _context;

        public Repository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<T> GetAsync<T>(object id) where T : class
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task InsertAsync<T>(T entity) where T : class
        {
            await _context.Set<T>().AddAsync(entity);
        }

        public async Task UpdateAsync<T>(T entity) where T : class
        {
            await Task.Factory.StartNew(()=>{ _context.Set<T>().Attach(entity); });

            _context.Entry(entity).State = EntityState.Modified;
        }

        public async Task DeleteAsync(object id)
        {
            var obj = await _context.Set<T>().FindAsync(id);
            
            await Task.Factory.StartNew(() => { _context.Set<T>().Remove(obj); });
        }
    }
}
