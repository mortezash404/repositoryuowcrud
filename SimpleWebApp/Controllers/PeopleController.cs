﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleWebApp.Entities;
using SimpleWebApp.Repositories;
using SimpleWebApp.UOW;

namespace SimpleWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        
        #region Dependency Injection

        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Person> _repository;

        #endregion

        #region Constructor

        public PeopleController(IUnitOfWork unitOfWork, IRepository<Person> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        } 

        #endregion

        
        // GET: api/People/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetPerson(int id)
        {
            var person = await _repository.GetAsync<Person>(id);

            if (person == null)
            {
                return NotFound();
            }

            return person;
        }

        // PUT: api/People/5
        
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPerson(int id, Person person)
        {
            if (id != person.Id)
            {
                return BadRequest();
            }

            await _repository.UpdateAsync(person);

            await _unitOfWork.CompleteAsync();

            return NoContent();
        }

        // POST: api/People
        
        [HttpPost]
        public async Task<ActionResult<Person>> PostPerson(Person person)
        {
            await _repository.InsertAsync(person);

            await _unitOfWork.CompleteAsync();

            return CreatedAtAction("GetPerson", new { id = person.Id }, person);
        }

        // DELETE: api/People/5

        [HttpDelete("{id}")]
        public async Task DeletePerson(int id)
        {
            await _repository.DeleteAsync(id);

            await _unitOfWork.CompleteAsync();
        }
    }
}
