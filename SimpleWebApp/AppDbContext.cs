﻿using Microsoft.EntityFrameworkCore;
using SimpleWebApp.Entities;

namespace SimpleWebApp
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");

            modelBuilder.Entity<Person>().Property(p => p.Name).HasMaxLength(20);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
