﻿using System.Threading.Tasks;

namespace SimpleWebApp.UOW
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}
